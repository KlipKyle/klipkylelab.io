#+STARTUP: showall
#+OPTIONS: toc:nil num:nil html-preamble:nil html-postamble:nil html-scripts:nil
#+TITLE: St. IGNUcious versus St. Philomena
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="../style.css" />

#+BEGIN_navbar
[[file:../index.org][/]] [[file:../blog.org][Blog]] [[file:ignucious-vs-philomena.org][IGNUcious vs Philomena]]
#+END_navbar

Published: [2020-08-29 Sat]

* Context

  A few weeks ago, I mentioned on Reddit how I learned GNU Emacs in
  part to take my mind off of an emotional breakup.  Someone replied
  with the following comment.

  #+begin_quote
  May Saint IGNUcious and the church of EMACS bless you in your
  further romantic endeavors.
  #+end_quote

  (The Reddit user is referring to the [[https://www.stallman.org/saint.html][Church of Emacs]], which Richard
  Stallman, founder of the GNU project and Free Software Foundation,
  started as a parody religious movement.  The name iGNUcious is a pun
  on the name Saint Ignatius of Loyola.)

  Well, I belong to a serious church with serious saints, so I wasn’t
  quite sure how to feel about this comment.  I knew it was a joke, so I
  felt like I should respond with something witty.  However, nothing
  witty came -- until the release of Emacs 27.1 on 11 August, which
  happened to coincide with the feast of Saint Philomena.

* Saint Who?  Saint Philomena

  I cannot help but wonder if it was more than coincidence that the
  [[file:emacs-27.1.org][Emacs 27.1 release]] was cut on feast day of Saint Philomena.

  [[http://www.philomena.org/patroness.asp#The_Story_of_St._Philomena][St. Philomena’s story]] recently reached out to me and touched my soul
  in a deep way that I cannot easily explain.  I encourage you to read
  her full life story as expressed in the nun’s vision.  The short
  version: At age 13, Philomena’s father arranged her to marry Emperor
  Diocletian as part of a deal to preserve peace in her Greek state.
  Philomena had made a vow of virginity at age 11 (similar to the vows
  of celibacy that the religious make today), so she refused
  Diocletian’s marriage proposal.  Refusing to take “no” for an
  answer, Diocletian imprisoned Philomena and ordered her flayed.  She
  healed miraculously overnight.  The next day, Diocletian tried to
  drown Philomena in the river, but she was miraculously saved.  Then,
  Diocletian ordered arrows to be shot into her, but Philomena
  miraculously healed again.  Diocletian tried again, but the arrows
  deflected and struck the archers.  Finally, Diocletian beheaded
  Philomena.  (Bear in mind that Philomena was a 13 year old girl when
  all this happened.)

  I have admiration for Richard Stallman and the GNU project for their
  fight for libre[fn:1] software.  Nowadays almost every software
  developer is using the GNU compiler whether he knows it or not.
  Linux (or GNU/Linux as Stallman prefers to call it) has taken over
  datacenters and phones around the world.

  However, Stallman’s story pales in comparison to Philomena’s
  strength of will.  Stallman gave up a normal career to create a
  libre operating system.  Philomena gave up her royal life as an
  empress, endured horrible tortures at the age of 13, and died
  because of her dedication to what she believed in.  (Also, Philomena
  is only one of many Christian saints with equivalently powerful
  stories.)

  I’m sorry, but there is no contest.

| St. IGNUcious (Richard Stallman)[fn:2]                   | St. Philomena[fn:3]                                                                                   |
|----------------------------------------------------------+-------------------------------------------------------------------------------------------------------|
| file:ignucious-vs-philomena/rms-350.jpg                  | file:ignucious-vs-philomena/philomena-350.jpg                                                         |
|----------------------------------------------------------+-------------------------------------------------------------------------------------------------------|
| Self-proclaimed saint in the [[https://www.stallman.org/saint.html][Church of Emacs]]             | Recognized as a saint by the Roman Catholic Church                                                    |
| “Lone wolf” of MIT                                       | Greek princess                                                                                        |
| Prophetic look                                           | Beautiful young girl                                                                                  |
| Stare into eyes: nothing happens                         | Stare into eyes: your soul melts with beauty and adoration                                            |
| Crown of gray hair                                       | Crown of lilies, symbolizing virginity and martyrdom                                                  |
| Carries Lemote, symbolizing software freedom             | Carries arrows and palm fronds, symbolizing virginity and martyrdom                                   |
| Refuses to use anything from Microsoft, Apple, or Amazon | [[http://www.philomena.org/patroness.asp#The_Story_of_St._Philomena][Refused to give herself to Diocletian]]: “I neither care for you as a lover, nor fear you as a tyrant.” |
| Tried to impeach God                                     | Chose the Divine Spouse over Diocletian                                                               |
| Willingly sacrificed job and career at MIT               | Willingly sacrificed life as an empress                                                               |
| Advocates [[https://www.stallman.org/articles/quantum-abortion.html][abortion]][fn:4] and [[https://www.stallman.org/articles/children.html][antinatalism]]                | Saves small children from danger                                                                      |
| Ate mystery substance from foot[fn:5]                    | Ate the body of Christ                                                                                |
| Devotions: removal of MS Windows, using Emacs            | Devotion: [[http://philomena.us/cord-saint-philomena/][the cord]] (to preserve chastity)                                                             |
| Can’t cure his own hand pain                             | Several miraculous healings, heals others too                                                         |

* Conclusion

  I’m not offended, but I’m going to stick with Christian saints.
  Plus, Philomena is adorable.  :-)

* Feedback

  + [[file:../about.org][Contact me]]
  + [[https://www.linkedin.com/feed/update/urn:li:activity:6705660604063084544/][LinkedIn thread]]

* Footnotes

[fn:1] I’m from southern California.  Around here, “libre” carries the
same definition of “free-as-in-freedom,” except “libre” is much easier
to say.

[fn:2] Image credit: [[https://en.wikipedia.org/wiki/File:Richard_Stallman_by_gisleh_01.jpg][Wikipedia]]

[fn:3] For high resolution images, see [[http://www.catholictradition.org/Saints/philomena.htm][catholictradition.org]]

[fn:4] I do give Stallman credit for the novelty of his pro-abortion
argument.  As someone who has examined both sides of the issue, I have
never seen an argument that applies quantum mechanics to the question
of whether an entity has a soul.

[fn:5] If you are fortunate enough to have never seen this video, then
consider yourself lucky.
