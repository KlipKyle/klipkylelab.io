#+STARTUP: showall
#+OPTIONS: toc:nil num:nil html-preamble:nil html-postamble:nil html-scripts:nil
#+TITLE: What the Tech World Can Learn from St. Philomena
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="../style.css" />

#+BEGIN_navbar
[[file:../index.org][/]] [[file:../blog.org][Blog]] [[file:philomena.org][Philomena]]
#+END_navbar

Published: [2020-09-07 Mon]

file:philomena/philomena-350.jpg

* Who is Saint Philomena?[fn:1]

  I have mentioned Saint Philomena a couple of times on this blog,
  once in comparison to [[file:ignucious-vs-philomena.org][St. IGNUcious]].  Another was in reference to
  the day on which [[file:emacs-27.1.org][Emacs 27.1]] was released (her feast day).

  However, mentioning St. Philomena in passing does her a disservice.
  She deserves an article of her own, perhaps several.

  Saint Philomena is a young virgin and martyr venerated in the
  Catholic Church.  She is more obscure than other saints.  However,
  she is still, as the slogan goes, “powerful with God.”

  The reason I want to focus on Saint Philomena is because she has
  pulled my heartstrings recently in a very special and personal way.
  Learning about her and adopting devotions to her have reinvigorated
  my spirit.  She has reminded me that during this troubling
  sociopolitical time, there are more important things to ponder and
  true beauty to admire.

  I encourage everyone to read Saint Philomena’s [[http://www.philomena.org/patroness.asp#The_Story_of_St._Philomena][life’s story]].  The
  short version:

  #+begin_quote
  At age 13, Philomena’s father arranged her to marry Emperor
  Diocletian as part of a deal to preserve peace in her Greek state.
  Philomena had made a vow of virginity at age 11 (similar to the vows
  of celibacy that the religious make today), so she refused
  Diocletian’s marriage proposal.  Refusing to take “no” for an
  answer, Diocletian imprisoned Philomena and ordered her flayed.  She
  healed miraculously overnight.  The next day, Diocletian tried to
  drown Philomena in the river, but she was miraculously saved.  Then,
  Diocletian ordered arrows to be shot into her, but Philomena
  miraculously healed again.  Diocletian tried again, but the arrows
  deflected and struck the archers.  Finally, Diocletian beheaded
  Philomena.  (Bear in mind that Philomena was a 13 year old girl when
  all this happened.)
  #+end_quote

* Bravery

  #+begin_quote
  I neither care for you as a lover, nor fear you as a tyrant.

  -- St. Philomena (to Emperor Diocletian)
  #+end_quote

  Can you imagine being subjected to sexual advances and flaying on
  the same day?  (Watch Mel Gibson’s [[https://www.imdb.com/title/tt0335345/][/Passion of the Christ/]] for a
  graphic video demonstration of what flaying is.)  Philomena
  experienced both when she was 13.  She was very brave to follow
  through on her decision to keep faith.

  All our struggles pale in comparison.

* Sense of bigger picture

  Philomena was a Greek princess.  She could have lived a perfectly
  happy life of ease with Diocletian.  However, she willingly traded
  that life for the bitter cup of torture and death.  She did so
  because of the strength of her beliefs.

  Why did she make such a sacrifice at such a high cost?  What did she
  see?

  To Philomena, her royal heritage meant nothing compared to her
  heritage and glory in Heaven as a Christian.

  Where is our sense of the bigger picture?  Is our technology really
  advancing forward?

* Humility

  I have said repeatedly that I believe the cardinal sin in the tech
  world is pride.  Every person tries to “one-up” everybody else.

  On the contrary, Philomena sacrificed her earthly royalty and humbly
  submitted herself to the Divine will.  She died and was forgotten by
  history for a thousand years until her body was discovered again in
  the catacombs.  She then worked miracles from the grave on anyone
  who asked for help.

  Philomena lives a quiet life, even as a saint.  In fact, her feast
  day was removed from most calendars in the 1960s.  Nevertheless, she
  still humbly intercedes and protects.

* Purity

  Philomena took a pledge of virginity at age 11.  She saw something
  more important than sex or marriage and decided to save herself,
  body and spirit, for Jesus in what the Catholic Church calls the
  Great Wedding Feast in Heaven.

  [[http://philomena.us/cord-saint-philomena/][The cord]] is a popular devotion to Philomena today.  It is a simple
  devotion.  The matter consists of a red and white cord tied around
  the waist.  The cord symbolizes chastity and tying your trust to
  Philomena.  Much like how Philomena saved herself as a gift to God,
  the cord allows you to “wrap” yourself as a gift (to God or a
  spouse).  There are various prayers and indulgences associated with
  the cord.

  Compare Philomena’s example of purity to the tech world, where
  people increasingly use sex appeal to grab attention.

  You cannot read news today without a suggestive image assailing your
  eyes.  Hardcore pornography is rampant on the Internet (especially
  while everyone is behind locked doors during these troubling times),
  and many people fight temptation every day.

  Furthermore, people who peddle pornography have [[https://davidwalsh.name/pornhub-interview][no shame]][fn:2] about
  what they do:

  #+begin_quote
  Q: In as far as end product, sharing that you work on adult sites
  may not be the same as working at a local web agency.  Is there a
  stigma attached to telling friends, family, and acquaintances you
  work on adult sites?  Is there any hesitance in telling people you
  work on adult sites?

  A: I’m very proud to work on these products, those close to me are
  aware and fascinated by it.  It’s always an amazing source of
  conversation, jokes and is genuinely interesting.
  #+end_quote

  (Saint Philomena, pray for us!)

* Beauty

  #+begin_quote
  Any sufficiently advanced technology is indistinguishable from
  magic.  -- Arthur C. Clarke
  #+end_quote

  When was the last time you were surprised with technology that was
  truly novel in the sense that it seemed magical?

  In recent years, computer technology has become less creative and
  more methodical.  Consumer phones look and act more or less the
  same.  Websites look and act more or less the same.  There is a
  status quo, and people follow trends like fashions.

  At some point, we lost that sense of beauty and novelty in
  technology.  People started to stare into their cold glass phone
  monitors and stopped admiring the people, world, and technology
  around them.

  We have created virtual prisons for our minds.

  On the contrary, [[http://www.philomena.org/holycards.asp][images of Philomena]] are pictures of the most
  beautiful girl I have ever seen.  If you stare into her eyes, she
  stares into your soul.  And these are just images of the actual
  person.

  The cord is a very simple object that when blessed has sacramental
  value.  Her [[http://philomena.us/chaplet-saint-philomena/][little chaplet]] looks like a single-decade rosary.
  However, it has 13 beads instead of the standard 10.  (Try finding a
  prayer chain with 13 beads!)  Both are unique and novel instruments.

  Stories about Philomena straddle the realm of fantasy: princesses,
  Roman persecution, miracles, and “Daughter of light.”  Stories about
  [[http://philomena.us/the-miracles/][miracles attributed to Philomena]] are equally fantastic (and church
  authorities have officially investigated a few of them): miraculous
  healings and children being saved from dangerous situations.

  Everything about Philomena is beautiful: images, devotions, the
  stories she tells, and the ways she renders miracles.  As tech
  people we can learn a lot from her good taste.

* Conclusion

  If I sound like I am describing a close friend, it is because I am.
  To me, Saint Philomena is a very good friend and a powerful
  protector.  I am very glad I have met her, and I think there is a
  lot her example can teach the tech world, as well as the rest of the
  world.

* Prayer

  The prayer of the cord:

  #+begin_verse
  O Saint Philomena, virgin and martyr, pray for us
  So that through your powerful intercession
  We may receive the purity of spirit and heart
  Which leads to the perfect love of God.
  Amen.
  #+end_verse

* Feedback

  + [[file:../about.org][Contact me]]
  + [[https://www.linkedin.com/feed/update/urn:li:activity:6708864786035306496/][LinkedIn thread]]

* Footnotes

[fn:1] For high resolution images, see [[http://www.catholictradition.org/Saints/philomena.htm][catholictradition.org]]

[fn:2] At the moment I am writing this, that link is safe for work.
It is an interview with a developer who works for a popular
pornography website.  Most of the interview is technical, so if you
are curious about how massive amounts of data are sent across the
Internet, then go ahead and read the whole thing.  Sadly, the moral
questions are relegated to the end as an afterthought.

