#+STARTUP: showall
#+OPTIONS: toc:t num:t html-preamble:nil html-postamble:nil html-scripts:nil
#+TITLE: Miscellaneous
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="../style.css" />

#+BEGIN_navbar
[[file:../index.org][/]] [[file:../faq.org][FAQ]] [[file:misc.org][Misc]]
#+END_navbar

* Details on computer setup?

+ Desktop: 10th generation Intel NUC NUC10i7FNH, 6-core Intel i7, 32GB
  RAM, 1TB NVMe SSD, USB 3.0 external disk for long term storage.  The
  Intel NUCs are powerful and very quiet.
+ Laptop (and backup machine): System 76 Gazelle Pro 8, which I have
  had since college.  4-core Intel Ivy Bridge i7, 16GB RAM, SATA SSD.
  Still a good system!
+ [[https://www.pckeyboard.com/][Unicomp keyboard]]: very loud, very tactile, and very satisfying.
+ Trackballs: Logitech TrackMan Wheel (red ball, no longer
  manufactured), Logitech MX Ergo (gray ball).  Trackballs rule!
