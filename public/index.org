#+STARTUP: showall
#+OPTIONS: toc:nil num:nil html-preamble:nil html-postamble:nil html-scripts:nil
#+TITLE: klipkyle.gitlab.io
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="style.css" />

#+BEGIN_navbar
[[file:index.org][/]]
#+END_navbar

#+ATTR_HTML: :class warning
#+begin_quote
*Dear user*: Most of this website has moved to [[https://terren.us/][terren.us]].  Please
update your bookmarks.
#+end_quote

Welcome to klipkyle.gitlab.io, home of many interesting things.

+ [[file:about.org][About]]
+ [[file:blog.org][Blog]]
+ [[file:faq.org][FAQ]]
+ [[file:kb.org][KB]]
+ [[file:projects.org][Projects]]
+ [[file:talks.org][Talks]]

#+begin_quote
Grant me chastity and self-control, but, please, not just yet.

-- St. Augustine
#+end_quote

#+begin_quote
I neither care for you as a lover, nor fear you as a tyrant.

-- St. Philomena (to Emperor Diocletian)
#+end_quote

#+BEGIN_QUOTE
The computer can't tell you the emotional story.  It can give you the
exact mathematical design, but what's missing is the eyebrows.

-- Frank Zappa
#+END_QUOTE

#+BEGIN_QUOTE
You can get everything in life you want if you will just help enough
other people get what they want.

-- Zig Ziglar
#+END_QUOTE

#+BEGIN_QUOTE
Without work, it’s impossible to have fun.

-- St. Thomas Aquinas
#+END_QUOTE

Tip: For an Emacs Org-Mode source version of any page, simply change
the =.html= suffix to =.org=.
